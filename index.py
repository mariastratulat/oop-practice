class Bag(object):

    def __init__(self):
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    def remove_item(self, item):
        self.items.remove(item)

    def list_bag(self):
        for item in self.items:
            print(item)


bag = Bag()

bag.add_item('cabbage')
bag.add_item('salmon')
print(bag.__dict__)

bag.remove_item('salmon')
bag.list_bag()
print(bag.__dict__)


# Same thing functional programming
bag_items = []


def add_items(item):
    global bag_items
    bag_items.append(item)


def remove_item(item):
    global bag_items
    bag_items.remove(item)


def list_bag():
    for item in bag_items:
        print(item)


add_items('cd')
add_items('some')
remove_item('cd')
list_bag()
